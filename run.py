from flask import Flask, jsonify, request
app = Flask(__name__)

@app.route('/')
def Home():
 return 'Halaman Home'


@app.route('/auth', methods=['GET', 'POST'])
def login():
 if request.method == 'POST':
  return jsonify({
   'data': 'return data',
   'is_success': True
  })
 
 return jsonify({
  'data': None,
  'message': 'Anda request GET',
  'is_success': False
 }) 

if __name__ == '__main__':
 app.run()
